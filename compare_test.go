package money

import (
	"fmt"
	"testing"
)

func TestCompareFloatAndIntValuesInStruct(t *testing.T) {
	type args struct {
		s interface{}
	}

	type forTest struct {
		Amount        float64
		AmountIntA    Money
		Fee           float64
		FeeIntA       int64
		Overdraft     float64
		OverdraftIntA Money
	}

	tests := []struct {
		name string
		args args
		want [1]error
	}{
		{
			"test1",
			args{forTest{
				Amount:        50.00,
				AmountIntA:    6000,
				Overdraft:     11,
				OverdraftIntA: 1100,
			}},
			[1]error{fmt.Errorf("Some error message")},
		}, {
			"test2",
			args{&forTest{}},
			[1]error{fmt.Errorf("Some error message")},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := CompareFloatAndIntValuesInStruct(tt.args.s)
			if (len(got) != len(tt.want)) || (len(got) != 1) {
				t.Errorf("CompareFloatAndIntValues() = %v, want %v", got, tt.want)
			}
		})
	}
}
