package money

import (
	"fmt"
	"strconv"

	"bitbucket.org/is-avdeev/money/rate"
	"bitbucket.org/is-avdeev/money/round"
)

type Money int64

func FromFloat(val float64) Money {
	var roundedVal float64

	if val < 0.0 {
		roundedVal = round.FirstWay(val*100, 2)

	} else {
		roundedVal = round.FirstWay(val, 2) * 100
	}

	return FromInt(int64(roundedVal))
}

func FromString(val string) Money {
	floatVal, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return Money(0)
	}

	return FromFloat(floatVal)
}

func FromInt(val int64) Money {
	return Money(val)
}

func (s Money) Int() int64 {
	return int64(s)
}

func (s Money) Float() float64 {
	return float64(s) / 100
}

func (s Money) String() string {
	return fmt.Sprintf("%.2f", s.Float())
}

func (s Money) Equal(val float64) bool {
	str1 := s.String()
	str2 := fmt.Sprintf("%.2f", val)
	return str1 == str2
}

func (s Money) ConvertAtRate(r rate.Rate) Money {
	val := s.Float() / r.Float()
	return FromFloat(val)
}

func (s Money) CalculateRate(val Money) rate.Rate {
	return rate.FromFloat(s.Float() / val.Float())
}
