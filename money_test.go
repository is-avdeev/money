package money

import (
	"reflect"
	"testing"

	"bitbucket.org/is-avdeev/money/rate"
)

func TestMoney_Equal(t *testing.T) {
	type args struct {
		val float64
	}
	tests := []struct {
		name string
		s    Money
		args args
		want bool
	}{
		{"test1", FromFloat(1.05), args{1.056}, false},
		{"test2", FromFloat(1.05), args{1.05090009}, true},
		{"test3", FromFloat(1.056), args{1.06}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Equal(tt.args.val); got != tt.want {
				t.Errorf("Money.Equal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMoney_String(t *testing.T) {
	tests := []struct {
		name string
		s    Money
		want string
	}{
		{"test1", FromInt(105), "1.05"},
		{"test2", FromFloat(1.0599999), "1.06"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.String(); got != tt.want {
				t.Errorf("Money.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMoney_Int(t *testing.T) {
	tests := []struct {
		name string
		s    Money
		want int64
	}{
		{"test1", FromFloat(1.0599999), 106},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Int(); got != tt.want {
				t.Errorf("Money.Int() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMoney_ConvertAtRate(t *testing.T) {
	type args struct {
		r rate.Rate
	}
	tests := []struct {
		name string
		s    Money
		args args
		want Money
	}{
		{"test1", FromFloat(100.00), args{rate.FromInt(730000)}, FromFloat(1.37)},
		{"test2", FromFloat(1000.00), args{rate.FromFloat(83.5932)}, FromFloat(11.96)},
		{"test3", FromFloat(1000.00), args{rate.FromInt(835932)}, FromFloat(11.96)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.ConvertAtRate(tt.args.r); got != tt.want {
				t.Errorf("Money.ConvertAtRate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFromString(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name string
		args args
		want Money
	}{
		{"test1", args{"370370.375"}, Money(37037038)},
		{"test2", args{"255974.4"}, Money(25597440)},
		{"test3", args{"-2.28"}, Money(-228)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromString(tt.args.val); got != tt.want {
				t.Errorf("FromString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMoney_CalculateRate(t *testing.T) {
	type args struct {
		val Money
	}
	tests := []struct {
		name string
		s    Money
		args args
		want rate.Rate
	}{
		// 1 to 4
		{"test1", FromFloat(1000.0), args{FromFloat(250.0)}, rate.FromFloat(4)},
		// 7962.74 RUR to 100.00 USD = 79.6274 (real april rate)
		{"test2", FromFloat(7962.74), args{FromFloat(100.0)}, rate.FromFloat(79.6274)},
		// 100.00 RUR to 1.26 USD = 79.6274 (real april rate)
		{"test3", FromFloat(100.33), args{FromFloat(1.26)}, rate.FromFloat(79.6274)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.CalculateRate(tt.args.val); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Money.CalculateRate() = %v, want %v", got, tt.want)
			}
		})
	}
}
