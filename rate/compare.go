package rate

import (
	"errors"
	"fmt"
	"reflect"
)

var ErrInvalidArg = errors.New("argument of non-struct type")

func CompareFloatAndIntValuesInStruct(s interface{}) (results []error) {
	results = make([]error, 0)

	defer func() {
		if recoveryMessage := recover(); recoveryMessage != nil {
			results = append(results, fmt.Errorf("%v", recoveryMessage))
		}
	}()

	t := reflect.TypeOf(s)
	v := reflect.ValueOf(s)

	// Из всех полей структуры выбираем значения типа float64
	for i := 0; i < t.NumField(); i++ {
		if v.Field(i).Kind() == reflect.Float64 {
			fieldName := t.Field(i).Name
			fieldIntAName := fieldName + "IntB"
			fieldIntA, found := t.FieldByName(fieldIntAName)
			if !found || (fieldIntA.Type.Name() != "Rate") {
				continue
			}

			valueIntB := Rate(v.FieldByName(fieldIntAName).Int())
			valueFloat := v.Field(i).Float()
			if !valueIntB.Equal(valueFloat) {
				results = append(results, fmt.Errorf(
					"DIFF_INT_FLOAT: %s = %v NOT EQUAL %s = %v", fieldName, valueFloat, fieldIntAName, valueIntB))
			}
		}
	}

	return results
}
