package rate

import (
	"fmt"
	"testing"
)

func TestCompareFloatAndIntValuesInStruct(t *testing.T) {
	type args struct {
		s interface{}
	}

	type forTest struct {
		Rate              float64
		RateIntB          Rate
		RecipientRate     float64
		RecipientRateIntB Rate
	}

	tests := []struct {
		name string
		args args
		want [1]error
	}{
		{
			"test1",
			args{forTest{
				Rate:              1.000099999,
				RateIntB:          10000,
				RecipientRate:     11.55,
				RecipientRateIntB: 115500,
			}},
			[1]error{fmt.Errorf("Some error message")},
		}, {
			"test2",
			args{&forTest{}},
			[1]error{fmt.Errorf("Some error message")},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := CompareFloatAndIntValuesInStruct(tt.args.s)
			if (len(got) != len(tt.want)) || (len(got) != 1) {
				t.Errorf("CompareFloatAndIntValues() = %v, want %v", got, tt.want)
			}
		})
	}
}
