package rate

import (
	"fmt"
	"strconv"

	"bitbucket.org/is-avdeev/money/round"
)

type Rate int64

func FromFloat(val float64) Rate {
	return Rate(int64(round.FirstWay(val, 4.0) * 10000))
}

func FromInt(val int64) Rate {
	return Rate(val)
}

func FromString(val string) Rate {
	floatVal, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return Rate(0)
	}

	floatVal = round.FirstWay(floatVal, 4)
	return Rate(int64(floatVal * 10000))
}

func (s Rate) Int() int64 {
	return int64(s)
}

func (s Rate) Float() float64 {
	return float64(s) / 10000
}

func (s Rate) String() string {
	return fmt.Sprintf("%.4f", s.Float())
}

func (s Rate) Equal(val float64) bool {
	str1 := s.String()
	str2 := fmt.Sprintf("%.4f", val)
	return str1 == str2
}

func (s *Rate) RecalculateInUnits(unit int) {
	*s = FromFloat(s.Float() / float64(unit))
}
