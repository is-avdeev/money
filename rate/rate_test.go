package rate

import (
	"testing"
)

func TestFromString(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name string
		args args
		want Rate
	}{
		{"test1", args{"74.0740737915039"}, Rate(740741)},
		{"test2", args{"76.100266"}, Rate(761003)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromString(tt.args.val); got != tt.want {
				t.Errorf("FromString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRate_RecalculateInUnits(t *testing.T) {
	testRate := FromFloat(1.2345)
	type args struct {
		unit int
	}
	tests := []struct {
		name string
		s    Rate
		args args
	}{
		{"test1", testRate, args{10}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.s.RecalculateInUnits(tt.args.unit)
			if tt.s != testRate {
				t.Errorf("RecalculateInUnits() = %v, want %v", tt.s, testRate/10)
			}
		})
	}
}
