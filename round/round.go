package round

import "math"

func FirstWay(value, decimalPlace float64) float64 {
	decimals64 := math.Pow(10, decimalPlace)
	multipliedRoundedNumber := math.Round(value * decimals64)
	return multipliedRoundedNumber / decimals64
}

func SecondWay(x, decimalPlace float64) float64 {
	const (
		mask  = 0x7FF
		shift = 64 - 11 - 1
		bias  = 1023

		signMask = 1 << 63
		fracMask = (1 << shift) - 1
		halfMask = 1 << (shift - 1)
		one      = bias << shift
	)

	multiplier := math.Pow(10, decimalPlace)
	x *= multiplier

	bits := math.Float64bits(x)
	e := uint(bits>>shift) & mask
	switch {
	case e < bias:
		// Round abs(x)<1 including denormals.
		bits &= signMask // +-0
		if e == bias-1 {
			bits |= one // +-1
		}
	case e < bias+shift:
		// Round any abs(x)>=1 containing a fractional component [0,1).
		e -= bias
		bits += halfMask >> e
		bits &^= fracMask >> e
	}

	return (math.Float64frombits(bits)) / multiplier
}

func ThirdWay(x, decimalPlace float64) float64 {
	if math.IsNaN(x) {
		return x
	}
	if x == 0.0 {
		return x
	}

	multiplier := math.Pow(10, decimalPlace)
	x *= multiplier

	roundFn := math.Ceil
	if math.Signbit(x) {
		roundFn = math.Floor
	}

	xOrig := x
	x -= math.Copysign(0.5, x)

	if x == 0 || math.Signbit(x) != math.Signbit(xOrig) {
		return (math.Copysign(0.0, xOrig)) / multiplier
	}

	if x == xOrig-math.Copysign(1.0, x) {
		return xOrig / multiplier
	}

	r := roundFn(x)
	if r != x {
		return r / multiplier
	}

	return (roundFn(x*0.5) * 2.0) / multiplier
}

func FourthWay(input, decimalPlace float64) float64 {
	if math.IsNaN(input) {
		return math.NaN()
	}

	multiplier := math.Pow(10, decimalPlace)
	input *= multiplier

	sign := 1.0
	if input < 0 {
		sign = -1
		input *= -1
	}

	_, decimal := math.Modf(input)
	var rounded float64
	if decimal >= 0.5 {
		rounded = math.Ceil(input)
	} else {
		rounded = math.Floor(input)
	}

	return (rounded * sign) / multiplier
}
