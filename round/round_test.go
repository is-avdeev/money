package round

import (
	"testing"
)

func TestFirstWay(t *testing.T) {
	type args struct {
		value        float64
		decimalPlace float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"test1", args{74.0740737915039, 4}, 74.0741},
		{"test2", args{370370.375, 2}, 370370.38},
		{"test3", args{255974.4, 2}, 255974.40},
		{"test4", args{76.100266, 4}, 76.1003},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FirstWay(tt.args.value, tt.args.decimalPlace); got != tt.want {
				t.Errorf("FirstWay() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSecondWay(t *testing.T) {
	type args struct {
		x            float64
		decimalPlace float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"test1", args{74.0740737915039, 4}, 74.0741},
		{"test2", args{370370.375, 2}, 370370.38},
		{"test3", args{255974.4, 2}, 255974.40},
		{"test4", args{76.100266, 4}, 76.1003},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SecondWay(tt.args.x, tt.args.decimalPlace); got != tt.want {
				t.Errorf("SecondWay() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestThirdWay(t *testing.T) {
	type args struct {
		x            float64
		decimalPlace float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"test1", args{74.0740737915039, 4}, 74.0741},
		{"test2", args{370370.375, 2}, 370370.38},
		{"test3", args{255974.4, 2}, 255974.40},
		{"test4", args{76.100266, 4}, 76.1003},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ThirdWay(tt.args.x, tt.args.decimalPlace); got != tt.want {
				t.Errorf("ThirdWay() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFourthWay(t *testing.T) {
	type args struct {
		input        float64
		decimalPlace float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"test1", args{74.0740737915039, 4}, 74.0741},
		{"test2", args{370370.375, 2}, 370370.38},
		{"test3", args{255974.4, 2}, 255974.40},
		{"test4", args{76.100266, 4}, 76.1003},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FourthWay(tt.args.input, tt.args.decimalPlace); got != tt.want {
				t.Errorf("FourthWay() = %v, want %v", got, tt.want)
			}
		})
	}
}
